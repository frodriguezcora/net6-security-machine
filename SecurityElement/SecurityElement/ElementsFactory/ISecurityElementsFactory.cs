﻿using SecurityElementsLibrary;
using SecurityElementsLibrary.AsciiMath;
using SecurityElementsLibrary.Custom;
using SecurityElementsLibrary.Reverse;

namespace CLI.ElementsFactory
{
    public interface ISecurityElementsFactory
    {
        IReverseSecurity Reverse();

        IAsciiMathSecurity AsciiMath();

        ICustomSecurity CustomLinqRecurisve();

        ISecurityElement GetElement(SecurityElementEnum option);
    }
}
