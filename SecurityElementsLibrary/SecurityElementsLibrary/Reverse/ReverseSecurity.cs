﻿namespace SecurityElementsLibrary.Reverse
{
    public class ReverseSecurity : BaseSecurityElement, IReverseSecurity
    {
        private string Apply(string text)
        {
            ValidateTextAndThrowIfFail(text);

            var copy = text
                .Reverse()
                .ToArray();

            return new string(copy);
        }

        public override string Decrypt(string text) => Apply(text);

        public override string Encrypt(string text) => Apply(text);
    }
}
