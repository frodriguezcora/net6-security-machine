﻿using SecurityElementsLibrary.AsciiMath.Decrypt;
using SecurityElementsLibrary.AsciiMath.Encrypt;

namespace SecurityElementsLibrary.AsciiMath
{
    public class AsciiMathSecurity : BaseSecurityElement, IAsciiMathSecurity
    {
        private readonly IAsciiMathDecrypt asciiMathDecrypt;
        private readonly IAsciiMathEncrypt asciiMathEncrypt;

        public AsciiMathSecurity(
            IAsciiMathDecrypt asciiMathDecrypt,
            IAsciiMathEncrypt asciiMathEncrypt)
        {
            this.asciiMathDecrypt = asciiMathDecrypt;
            this.asciiMathEncrypt = asciiMathEncrypt;
        }

        public override string Decrypt(string text)
        {
            ValidateTextAndThrowIfFail(text);

            var result = text
                .Select(character => asciiMathDecrypt.Execute(character))
                .ToArray();

            return new string(result);
        }

        public override string Encrypt(string text)
        {
            ValidateTextAndThrowIfFail(text);

            var result = text
                .Select(character => asciiMathEncrypt.Execute(character))
                .ToArray();

            return new string(result);
        }
    }
}
