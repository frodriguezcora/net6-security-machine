﻿namespace SecurityElementsLibrary.AsciiMath.Encrypt
{
    public class AsciiMathEncrypt : IAsciiMathEncrypt
    {
        private const int MaxAsciiValue = 255;
        private const int MinAsciiValue = 0;

        public char Execute(char character)
        {
            var ascii = (int)character;

            if (ascii.Equals(MaxAsciiValue))
            {
                return Convert.ToChar(MinAsciiValue);
            }

            return Convert.ToChar(ascii + 1);
        }
    }
}
