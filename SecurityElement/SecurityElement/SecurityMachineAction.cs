﻿using CLI.ElementsFactory;
using CLI.SecurityMachine.Builder;
using SecurityElementsLibrary;

namespace CLI
{
    public static class SecurityMachineAction
    {
        private const int Exit = 0;

        public static void Run()
        {
            //DEPENDENCIES (in the future we can use a DI resolver)
            var factory = new SecurityElementsFactory();
            var builder = new SecurityMachineBuilder();

            var textPayload = string.Empty;

            while (string.IsNullOrWhiteSpace(textPayload))
            {
                Console.Clear();
                Console.WriteLine("Please insert a not empty string and press intro...");
                textPayload = Console.ReadLine();
                Console.Clear();
            }

            Console.WriteLine("");
            Console.WriteLine("Security Element Options (will be applied in selected order)");
            Console.WriteLine("[1] Reverse");
            Console.WriteLine("[2] ASCII Math");
            Console.WriteLine("[3] CustomLinqRecurisve");
            Console.WriteLine("");
            Console.WriteLine("[Enter] TO FINISH");
            Console.WriteLine("");
            Console.WriteLine("Select a option...");

            var securityElementOption = FormatSecurityElementOption(Console.ReadLine());

            while (!securityElementOption.Equals(Exit))
            {
                var securityElement = factory.GetElement((SecurityElementEnum)securityElementOption);
                builder.AddSecurityElement(securityElement);

                Console.WriteLine("Select other option...");
                securityElementOption = FormatSecurityElementOption(Console.ReadLine());
            }

            var machine = builder.Build();

            Console.Clear();

            var encrypted = machine.Encrypt(textPayload);
            var decrypted = machine.Decrypt(encrypted);

            Console.WriteLine($"ORIGINAL: {textPayload}");
            Console.WriteLine($"ENCRYPTED: {encrypted}");
            Console.WriteLine($"DECRYPTED: {decrypted}");
        }

        private static int FormatSecurityElementOption(string payload) => string.IsNullOrWhiteSpace(payload) ?
                    Exit :
                    int.Parse(payload);
    }
}
