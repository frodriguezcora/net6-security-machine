﻿using SecurityElementsLibrary;

namespace CLI.SecurityMachine.Builder
{
    public interface ISecurityMachineBuilder
    {
        ISecurityMachineBuilder AddSecurityElement(ISecurityElement securityElement);

        ISecurityMachine Build();
    }
}
