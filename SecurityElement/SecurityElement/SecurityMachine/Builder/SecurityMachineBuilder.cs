﻿using SecurityElementsLibrary;

namespace CLI.SecurityMachine.Builder
{
    public class SecurityMachineBuilder : ISecurityMachineBuilder
    {
        private const string ErrorInvalidConfiguration = "The Security Machin must have Security Elements";
        private IEnumerable<ISecurityElement> SecurityElements = Enumerable.Empty<ISecurityElement>();

        public ISecurityMachineBuilder AddSecurityElement(ISecurityElement securityElement)
        {
            SecurityElements = SecurityElements.Append(securityElement);
            return this;
        }

        public ISecurityMachine Build()
        {
            if (!SecurityElements.Any())
            {
                throw new Exception(ErrorInvalidConfiguration);
            }

            var instance = new SecurityMachine(SecurityElements);
            SecurityElements = Enumerable.Empty<ISecurityElement>();

            return instance;
        }
    }
}
