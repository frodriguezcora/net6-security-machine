﻿using SecurityElementsLibrary;

namespace CLI.SecurityMachine
{
    public class SecurityMachine : ISecurityMachine
    {
        private readonly IEnumerable<ISecurityElement> SecurityElements;

        public SecurityMachine(IEnumerable<ISecurityElement> securityElements)
        {
            SecurityElements = securityElements;
        }

        public string Encrypt(string text)
        {
            var mutation = text;
            SecurityElements
                .ToList()
                .ForEach(encryptor => mutation = encryptor.Encrypt(mutation));

            return mutation;
        }

        public string Decrypt(string text)
        {
            var mutation = text;
            SecurityElements
                .Reverse()
                .ToList()
                .ForEach(decryptor => mutation = decryptor.Decrypt(mutation));

            return mutation;
        }
    }
}
