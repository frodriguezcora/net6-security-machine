﻿namespace SecurityElementsLibrary.Custom
{
    public class CustomSecurity : BaseSecurityElement, ICustomSecurity
    {
        private string Recursive(string text)
        {
            if (text.Length <= 1)
            {
                return text;
            }

            var mutation = new string(text
                .Remove(0, 1)
                .Remove(text.Length - 2, 1)
                .Reverse()
                .ToArray()
            );

            return $"{text.Last()}{Recursive(mutation)}{text.First()}";
        }

        public override string Decrypt(string text) => Recursive(text);


        public override string Encrypt(string text) => Recursive(text);
    }
}
