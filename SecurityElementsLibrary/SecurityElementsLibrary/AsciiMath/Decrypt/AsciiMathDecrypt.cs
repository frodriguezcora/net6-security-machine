﻿namespace SecurityElementsLibrary.AsciiMath.Decrypt
{
    public class AsciiMathDecrypt : IAsciiMathDecrypt
    {
        private const int MaxAsciiValue = 255;
        private const int MinAsciiValue = 0;

        public char Execute(char character)
        {
            var ascii = (int)character;

            if (ascii.Equals(MinAsciiValue))
            {
                return Convert.ToChar(MaxAsciiValue);
            }

            return Convert.ToChar(ascii - 1);
        }
    }
}
