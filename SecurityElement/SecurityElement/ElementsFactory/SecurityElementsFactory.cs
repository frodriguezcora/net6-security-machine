﻿using SecurityElementsLibrary;
using SecurityElementsLibrary.AsciiMath;
using SecurityElementsLibrary.AsciiMath.Decrypt;
using SecurityElementsLibrary.AsciiMath.Encrypt;
using SecurityElementsLibrary.Custom;
using SecurityElementsLibrary.Reverse;

namespace CLI.ElementsFactory
{
    public class SecurityElementsFactory : ISecurityElementsFactory
    {
        private const string ErrorInvalidSecurityElementOption = "The selected Secuirty Element is INVALID OPTION";

        public IReverseSecurity Reverse() => new ReverseSecurity();

        public IAsciiMathSecurity AsciiMath() => new AsciiMathSecurity(
            new AsciiMathDecrypt(),
            new AsciiMathEncrypt()
        );

        public ICustomSecurity CustomLinqRecurisve() => new CustomSecurity();

        public ISecurityElement GetElement(SecurityElementEnum option) => option switch
        {
            SecurityElementEnum.Reverse => Reverse(),
            SecurityElementEnum.AsciiMath => AsciiMath(),
            SecurityElementEnum.Custom => CustomLinqRecurisve(),
            _ => throw new Exception(ErrorInvalidSecurityElementOption)
        };
    }
}
