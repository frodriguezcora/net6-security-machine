﻿namespace SecurityElementsLibrary.AsciiMath.Decrypt
{
    public interface IAsciiMathDecrypt
    {
        char Execute(char character);
    }
}
