﻿namespace CLI.SecurityMachine
{
    public interface ISecurityMachine
    {
        string Encrypt(string text);

        string Decrypt(string text);
    }
}
