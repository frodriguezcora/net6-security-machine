﻿namespace SecurityElementsLibrary
{
    public abstract class BaseSecurityElement : ISecurityElement
    {
        private const string ErrorInvalidInput = "The string must be not empty";

        protected void ValidateTextAndThrowIfFail(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                throw new Exception(ErrorInvalidInput);
            }
        }

        public abstract string Decrypt(string text);

        public abstract string Encrypt(string text);
    }
}
