﻿namespace SecurityElementsLibrary.AsciiMath.Encrypt
{
    public interface IAsciiMathEncrypt
    {
        char Execute(char text);
    }
}
