﻿// See https://aka.ms/new-console-template for more information

using CLI;

try
{
    SecurityMachineAction.Run();
}
catch (Exception error)
{
    Console.WriteLine("An error ocurred, please try again.");
    Console.WriteLine(error.Message);
}

Console.ReadLine();